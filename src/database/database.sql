-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Naima
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-08 13:28
-- Created:       2021-12-08 13:28
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "skills"(
  "idskills" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "activityType" VARCHAR(45) NOT NULL,
  "title" VARCHAR(45) NOT NULL,
  "content" TEXT NOT NULL
);
CREATE TABLE "criteres"(
  "idcriteres" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" TEXT NOT NULL,
  "skills_id" INTEGER NOT NULL,
  CONSTRAINT "fk_criteres_skills1"
    FOREIGN KEY("skills_id")
    REFERENCES "skills"("idskills")
);
CREATE INDEX "criteres.fk_criteres_skills1_idx" ON "criteres" ("skills_id");
CREATE TABLE "users"(
  "idusers" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "email" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL,
  "created_at" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "modified_at" DATETIME NOT NULL,
  "deleted_at" DATETIME NOT NULL
);
CREATE TABLE "criteres_has_users"(
  "criteres_id" INTEGER NOT NULL,
  "users_id" INTEGER NOT NULL,
  PRIMARY KEY("criteres_id","users_id"),
  CONSTRAINT "fk_criteres_has_users_criteres1"
    FOREIGN KEY("criteres_id")
    REFERENCES "criteres"("idcriteres"),
  CONSTRAINT "fk_criteres_has_users_users1"
    FOREIGN KEY("users_id")
    REFERENCES "users"("idusers")
);
CREATE INDEX "criteres_has_users.fk_criteres_has_users_users1_idx" ON "criteres_has_users" ("users_id");
CREATE INDEX "criteres_has_users.fk_criteres_has_users_criteres1_idx" ON "criteres_has_users" ("criteres_id");
COMMIT;
