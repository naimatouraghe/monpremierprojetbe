import { Request, Response } from "express-serve-static-core";

export default class SkillsController

{
    static skills(req: Request, res: Response): void
    {
        const db =req.app.locals.db;
        const stmt = db.prepare('SELECT * FROM skills').all();
        
        res.render('pages/skills', {
            title: 'Compéténces',
            skills: stmt,
        });
    }

    
}