import { Application } from "express";
import HomeController from "./controllers/HomeController";
import SkillsController from "./controllers/SkillsController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) =>
    {
        HomeController.about(req, res);
    });

    app.get('/skills', (req, res) =>
    {
        SkillsController.skills(req, res);
    });
}
